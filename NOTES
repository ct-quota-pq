## -*- mode: org; -*-
This file contains semi-structured notes about devel process
It is no always reflect real progres of patch queue

* Quota todo list
** DONE Investigate XFS project id internals
   - It is possible to replace the root of project in rename() syscal.
   - It is not possible to disable cross project movements
** Add directory tree id support to ext4 inode
   : EXT4_MOUNT_TREE_ID 
   : EXT4_FEATURE_INCOMPAT_TREE_QUOTA
   : #define	EXT4_IOC_GET_TREEID		_IOR('f', 16, unsigned int)
   : #define	EXT4_IOC_SET_TREEID		_IOW('f', 17, unsigned int)
*** TODO Add directory tree_id support to lib2fs  tools
    1) commits:
       - commit cf0eeec0159b1af67fad9bccce220402cf08dff0
       - commit 88b34b1f87c2d7c3dfc9be7ff6e5d916f06d960a
       - commit c2d4300b8a4a13d8a78b86c386f76259f23feec2
	 
    2) extra
       commit 2921332fd88b843ffb828d9c18f05bfd171ace76
*** DONE Implement tree_id inheritance from dir to entry
    Done in ext4_new_inode() right before vfs_quota_init
*** DONE Prohibit bad operations like
**** rename
     *UPDATE* Due to locking issues cross-tires renames completely
     prohibited
    1) disallow for all types where ddir->tree\_id != sdir->tree\_id
    2) disallow if !DIR and i\_lnk != 1
    3) disallow if DIR and i\_lnk != 2
    Add *FIXME_NOTE* about quota transfer in cross tree rename() [1]
    
**** link
    allow only if src->tree\_id == dst->tree\_id
*** TODO Add runtime checks for inapropriate tree.
    In fact some one may turn-off TREE_ID mount flag and then mix up tries
    content. Later TREE_ID flag may passed on new mount (remount are not possible) 
    We have to check what tree inheritance assumption is ok.
**** DONE define inheritance assumptions
**** TODO find suitable place for checks [1/1]
     suitable place is following where we have parent dentry and child dentry
     The check must being done only once because it is not possible to change
     it back.
     Probably ext4_lookup is a good candidate for that.
     In case of error inode will be marked as containg bad quota.
     this may result in troubles because it may already contain quota info.
     In this case we have to print _huge_ warning msg and drop quota.
     - [X] ext4_lookup :: check for proper inheritance

*** Performance
**** open-close 100000, ext3 with usr+grp quota each thread run on didicated fs.
*Awful results*. Quota locking is fatally sequential. It is ten times slower
in case of 4 concurrent filesystems.
|-------------+-----------+------------|
| num threads | quota-off | w quota-on |
|           1 |   1792677 |    7353557 |
|             |           |            |
|           2 |   1888589 |    8345385 |
|             |   1954368 |    8401524 |
|             |           |            |
|           3 |   2176776 |   11969783 |
|             |   2300495 |   12422900 |
|             |           |            |
|           4 |   2475488 |   20070904 |
|             |   2651336 |   17373849 |
|-------------+-----------+------------|

**** It is strange but result depend on what user are execute tests :)
| test               |      ext3 | ext3+quota | su  ext3 | su ext3+quota |
| open-close 1000000 |   2380660 |    2475557 |  3375653 |       3645048 |
|                    |   2380660 |    2370699 |  3359800 |       3641397 |
|                    |   2360164 |    2455658 |  3528724 |       3642408 |
|                    |           |            |          |               |
| open-unlink 100000 |   1749507 |    2627881 |  1786597 |       4488170 |
|                    |   1751481 |    2611456 |  1788580 |       4405862 |
|                    |   1757022 |    2629494 |  1791130 |       4438595 |
|--------------------+-----------+------------+----------+---------------|

**** Comparison chart vanila and with tree-quota patch queue.                                    
EXT3
|--------------------+-------------+------------+---------+----------|
| test               | su  ext3    | w patch    |  ext3+q |  w patch |
| open-close 1000000 | 3375653     | 2377562    | 3645048 |  2969361 |
|                    | 3359800     | 2367956    | 3641397 |  3028390 |
|                    | 3528724     | 2401222    | 3642408 |  2966994 |
|                    |             |            |         |          |
| open-unlink 100000 | 1786597     | 1802290    | 4488170 |  2814249 |
|                    | 1788580     | 1785768    | 4405862 |  2827910 |
|                    | 1791130     | 1777920    | 4438595 |  2813790 |
|                    |             |            |         |          |
| chown 100000       | 2877498 /10 | 2861473/10 | 2782447 |  3872993 |
|                    | 2855129 /10 | 2868539/10 | 2805415 |  3783275 |
|                    | 2865346 /10 | 2858689/10 | 2799819 |  3894187 |
|                    |             |            |         |          |
| chown2 100000      | 2854466 /10 | 2867439/10 | 6691358 |  8453543 |
|                    | 2855050 /10 | 2869318/10 | 6412669 |  7932430 |
|                    | 2894878 /10 | 2930830/10 | 6638210 |  8097675 |
|                    |             |            |         |          |
| chown3 100000      | 5693838 /10 | 5724797/10 | 6957357 | 12402760 |
|                    | 5688717 /10 | 5791604/10 | 6526796 | 13011283 |
|                    | 5705545 /10 | 5721885/10 | 6894547 | 12890701 |
|--------------------+-------------+------------+---------+----------|
EXT4
|--------------------+------------+------------+------------+----------|
| test               | ext4       | w patch    | ext4+quota |  w patch |
| open-close 1000000 | 2572571    | 2548441    |    3831249 | 34207090 |
|                    | 2562564    | 2540445    |    3843340 | 40511598 |
|                    | 2568982    | 2570501    |    3902613 | 37835694 |
|                    |            |            |            |          |
| open-unlink 100000 | 2433148    | 2219039    |    4286613 | 31892179 |
|                    | 2456095    | 2228081    |    4307643 |  8150296 |
|                    | 2437422    | 2243730    |    4652567 | 19954407 |
|                    |            |            |            |          |
| chown 100000       | 3051037/10 | 2948017/10 |    2985798 |  6688243 |
|                    | 3041008/10 | 2935703/10 |    2984359 |  6141977 |
|                    | 3089298/10 | 2935703/10 |    3036337 |  6527266 |
|                    |            |            |            |          |
| chown2 100000      | 3024882/10 | 2980009/10 |    7211971 | 11540048 |
|                    | 3051953/10 | 2959488/10 |    7071395 | 10705118 |
|                    | 3018066/10 | 2966467/10 |    7000306 | 10888212 |
|                    |            |            |            |          |
| chown3 100000      | 6045590/10 | 5886467/10 |    6641102 | 14452187 |
|                    | 6045006/10 | 5922254/10 |    6579055 | 14987704 |
|                    | 6045006/10 | 5901298/10 |    6683252 | 15007360 |
|--------------------+------------+------------+------------+----------|


** Add trace points to quota subsys
   This helps in later debugging. And probably this will helps
   later code to be easily accepted in mainstream.
**** Define proper trace events
**** Add trace_points to quota code
** Add tree quota support to ext4
   : EXT4_MOUNT_TRQUOTA
*** DONE This about best way to implement quota tree		       :NOTE:
    In fact where are two possible ways to accomplish this task
**** Store inode id inside ondisk structure for each inode.
     Implement it as xfs does.
**** Store tree_id only for root nodes
     We will inherent tree\_id from parent on path_lookup and store it
     only in memory inode structure.
**** Comparison
| Characteristics      | ondisk                  | in memory                               |   |
|---------------------+-------------------------+-----------------------------------------+---|
| demadge resistance  | +Best(fsck)             | -Only in runtime                        |   |
| orphan list replay  | +No problem             | -Where is no parent at this time, so we |   |
|                     |                         | have to remember tree_id some where     |   |
| Disk layout changes | -Required               | +Not required                           |   |
| Performance         | -More disk IO           | + Minimum disk activity                 |   |
|                     | cross tree rename() [1] |                                         |   |

*** DONE Think about quota transfer interface
    Currently transfer get i_gid and i_uid from "struct iattr"
    so where is no place from i_treeid for
*** DONE perform quota transfer on rename
    Currently where is no quota transfer on rename, we have to perform
    it and if EDQUOT fail whole operation.
    - Dont forget to mark inode as dirty because of i_tree changes.

*** DONE Fix journal transaction block credits
    Most places has hardcoded usr+grp
*** Testing
**** ext4+quota [2/2]
     - [X] fsstress in different trees
     - [X] open-close / open-unlink / chown{,2,3}
     - [ ] seq quotaon/quotaoff	   
**** ext4+ journaled quota [3/4]
     - [X] fsstress in different trees
     - [X] open-close / open-unlink / chown{,2,3}	   
     - [X] seq quotaon/quotaoff
     - [ ] concurrent quotaon, quotaoff
     - [ ] mount / remount / concurent remount

**** Create testing tools [1/3]
     - [ ] Add tree id support to fsstress.
     - [X] add mv rename operation.
   
** Add separate tree/usr quota id for ext4
   This task must being done in following steps
*** Extend qutid_t type to 64 bit (Change all interfaces to use this bits)
**** TODO think about quotactl interface currently it support only 32bit id
*** Investigate quota file format v0
*** Create 64-bit quota file format
**** Testing
     It must being tested with fsstress like follows on ext4:
     : - decode id = (id << 32) + ~((int)id)
     : - decode id = (id << 32) + tree_id  


** footnotes
Footnotes: 
[1]  Cross tree rename. Usually we have to mark srcdir and dstdir
as dirty, but now we have to also mark inode itself because its tree id may
be changed

